import os,sys 

lib_path = os.path.abspath('./lib')
sys.path.append(lib_path)

from chime_monitor import *

# CHIME Monitor testing suite. This suite fakes the status of 
# the CHIME machines, and sees if the system identifes the 
# problems correctly.

chime_monitor = CHIMEMonitor()

# Register the CHIME west rack.

chime_west = RackMachine('config/chime-west.json')
chime_monitor.register_machine( chime_west )

tests = []

def PlenumTemperatureTest():

	chime_west.statistics['lower_temperature'] = None
	result = chime_west.evaluate()
	assert result.SEVERITY == FailureWarning.SUSPICIOUS.SEVERITY, Exception("Severity must be SUSPICIOUS for cold air.")
	chime_west.statistics['lower_temperature'] = '65'
	result = chime_west.evaluate()
	assert result.SEVERITY == FailureWarning.MILD.SEVERITY, Exception("Severity not mild.")
	chime_west.statistics['lower_temperature'] = '67.2'
	result = chime_west.evaluate()
	assert result.SEVERITY == FailureWarning.MEDIUM.SEVERITY, Exception("Severity not MEDIUM.")
	chime_west.statistics['lower_temperature'] = 75
	result = chime_west.evaluate()
	assert result.SEVERITY == FailureWarning.BAD.SEVERITY, Exception("Severity not BAD.")
	chime_west.statistics['lower_temperature'] = 99
	result = chime_west.evaluate()
	assert result.SEVERITY == FailureWarning.CRAZY.SEVERITY, Exception("Severity not CRAZY.")
	return True 

def GPUTemperatureTest():
	g = chime_west.machines[0]
	g.statistics['gpu_temperatures'] = [-15, 0, 0]
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.SUSPICIOUS.SEVERITY, Exception()
	g.statistics['gpu_temperatures'] = [81, 31.5, 20]
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.MILD.SEVERITY, Exception("result = %s" % result)
	g.statistics['gpu_temperatures'] = [95, 95, 95]
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.MEDIUM.SEVERITY, Exception()
	g.statistics['gpu_temperatures'] = [122, 85, 85]
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.BAD.SEVERITY, Exception()
	return True

def ComputerPowerTest():
	g = chime_west.machines[0] = GPUMachine('chi01')
	g.statistics['power'] = '150'
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.SUSPICIOUS.SEVERITY, Exception("result = %s" % result)
	g.statistics['power'] = '810'
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.MILD.SEVERITY, Exception("result = %s" % result)
	g.statistics['power'] = 870
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.MEDIUM.SEVERITY, Exception("result = %s" % result)
	g.statistics['power'] = 901
	result = g.evaluate()
	assert result.SEVERITY == FailureWarning.BAD.SEVERITY, Exception("result = %s" % result)
	return True


tests = [PlenumTemperatureTest, GPUTemperatureTest, ComputerPowerTest]
i = 0
for t in tests:
	if t():
		i+=1
		print "Test %d / %d passed." % (i,len(tests))

print "All tests passed."