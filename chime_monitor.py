# This program is supposed to keep watch of the
# CHIME computer racks, and signal a number of possible
# configurations. 

import time
import os, sys
from threading import Timer


from machine import *

# The following is an array of constants,
# which represent the way the program runs.


# The CHIMEMonitor class is the class which
# runs all of the monitoring. An instance of it
# is always alive while the program is running.
class CHIMEMonitor:
	def __init__(self):
		self.const = {
			"version": "0.1",
			"debug": False
		}

		self.version 	= self.const['version']
		self.start_date = time.asctime()
		self.machine	= LocalMachine(self)
		self.machines	= [ self.machine ]


	def log(self, message):
		if self.const['debug']:
			print "<%s>: %s" % (time.asctime(), message)

	def register_machine(self, machine):
		# Register the parent with us, so they can use
		# our log functions.
		machine.parent = self
		# And append the machine to our list.
		self.machines.append(machine)

	def validate_machines(self):
		for m in self.machines:
			if m.validate() == False:
				return False
		return True

	def take_action(self):
		for m in self.machines:
			m.action()

	def visualize(self):
		return '\n'.join( [ m.visualize() for m in self.machines] )

	def update(self):
		for m in self.machines:
			m.collect_statistics()
			m.evaluate()

	def render(self):
		me = []
		me.append(" - version %s" % self.version )
		me.append(" - started on: %s" % self.start_date)
		me.append(" - running on: %s" % self.machine.hostname)
		return '\n'.join(me)
