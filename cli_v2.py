# CHIME Monitor:
# Command-line interface

# Written by Colin Merkel, hacked by KV.

help = """
CHIME Monitor command-line interface.

The way to call this script is:

$ python cli.py <PATH TO RACK CONFIG FILE> [--node <HOSTNAME OF NODE>] [--get power]

For example,

$ python cli_v2.py --node chi01 --get power
$ python cli_v2.py --node chi02 --get gpu_temperature
$ python cli_v2.py --get air_temperature
$ python cli_v2.py --get power
"""

import os,sys 

lib_path = os.path.dirname( os.path.realpath(__file__) ) + '/lib'
sys.path.append(lib_path)

from chime_monitor import *
arguments = sys.argv[1:]


rack_configs = ['/usr/local/bin/snmp-scripts/config/chime-east.json','/usr/local/bin/snmp-scripts/config/chime-west.json']

try:
	racks = [RackMachine(rack_configs[0]),RackMachine(rack_configs[1])]
except:
	print "There was a problem loading the rack configuration files. "
	print help
	sys.exit(0)

# Now let's try to load up all of the configuration files.
node 		= None
data_name 	= None

for i in range(len(arguments)-1):
	if arguments[i] == "--node":
		node 		= arguments[i+1] 
	elif arguments[i] == "--get":
		data_name 	= arguments[i+1]

machine = None
if node != None:
	try:
		for rack in racks:
			machine = rack.get_machine_by_hostname(node);
			if (machine != None):
				break;
		if (machine == None):
			raise Exception()
	except:
		print "There was a problem with your hostname '%s'. Refer to your configuration file." % node
		print help
		sys.exit(0)

if data_name == "power":
	rack.collect_power_statistics()
	print rack.statistics['power'][ int(machine.power_socket) ]
elif data_name == "gpu_temperature":
	for n,temp in enumerate(machine.gpu_temperature()):
		print "gpu_temperature%d:%d"%(n,temp),
else:
	print "Uknown command. Refer to help."
	print help
	sys.exit(0)
