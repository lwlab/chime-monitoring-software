CHIME MONITORING SOFTWARE
-------------------------

The CHIME monitoring software is written in Python, and is used for interacting with the CHIME racks and GPU computers, as well as monitoring and automatically reacting to their temperature, power, and network state(s). 

CHIME is a new type of radio telescope installed in Penticton, B.C., which uses an on-site supercomputer called a "correlator" to process its signals. See http://chime.phas.ubc.ca/.

The CHIME monitoring software is intended to be run on the "root" node of CHIME, which controls all of the other nodes.

Features
--------

1. Can verify the network connection of all of the nodes
2. Can measure the power draw of each individual node, as well as fan power
3. Can turn each power switch on and off individually
4. Can measure the GPU temperatures for each adapter on each node
5. Can send commands to any node via SSH
6. Can measure the fan flow rates, temperatures, and more
7. Can automatically monitor nodes for defined failure modes, such as overheating, and take appropriate action (such as system shutdown, alerts, etc.)

Installation 
------------

You'll need the following Python libraries:

1. subprocess, threading
2. pycurl
3. urllib, urllib2

Then you'll need to set up the configuration files, under "/config/chime-east.json" and "/config/chime-west.json". Refer to the example file, "/config/rack-example.json" for an example of a configuration file. The configuration files define the mapping between CHIME node, hostname, and power socket for each rack. The configuration files use the JSON file format. When developing a configuration file, you can determine whether there are problems with the file by running it through an online JSON parser, such as this one: http://json.parser.online.fr/

Once configuration is defined, look at the code in "main.py" and make sure that the relevant racks that you would like to test are loaded into the chime_monitor object. This is set up lines 10-15, and has many comments.

Usage
-----

There are three main features of the CHIME monitoring software:

1. A python console interface (`console.py`)
2. A command-line interface, (`cli.py`)
3. A monitoring daemon-like software which keeps an eye on the machines (`main.py`)

Each component is discussed in the following section.

Console interface
-----------------

File: `console.py`

The console interface is for use in your own scripts, or in the python console. Start a python console in the repository directory, and then do:

 import console

You can then do debugging, etc. from the command-line. Refer to the code of `console.py` to see the default configuration files that you'll need to set up (chime-east.json and chime-west.json).

Command-line interface
----------------------

File: `cli.py`.

The command-line interface allows one to get individual values for the statistics of individual nodes via the command line, which allows for integration with packages like CACTI. Refer to the file `cli.py` for specific documentation of the features of the command-line interface.

The way to call this script is:

 $ python cli.py <PATH TO RACK CONFIG FILE> [--node <HOSTNAME OF NODE>] [--get power]

For example,

 $ python cli.py config/chime-west.json --node chi01 --get power
 $ python cli.py config/chime-west.json --node chi02 --get gpu_temperature
 $ python cli.py config/chime-east.json --get air_temperature
 $ python cli.py config/chime-east.json --get power

CHIME Monitor "daemon"
----------------------

File: `main.py`

This software watches the state of the machines, prints out a console interface to keep track of them, and automatically takes pre-determined actions when it senses that something is wrong. The system works using the following procedure:

1. Print out the current state of all machines to the screen
2. Interrogate each machine for all statistics (in parallel using seperate threads)
3. Evaluate the reported statistics against possible failure modes, defined in `/lib/failure_modes.py`
4. Based upon the severity of warnings reported by each failure mode, compile an overall state of each machine and rack
5. Take predetermined actions based upon the severity of the error, such as restarting the machine, logging an error, or shutting down all machines. 