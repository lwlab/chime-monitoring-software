# CHIME Monitor:
# Command-line interface

# Written by Colin Merkel.

help = """
CHIME Monitor command-line interface.

The way to call this script is:

$ python cli.py <PATH TO RACK CONFIG FILE> [--node <HOSTNAME OF NODE>] [--get power]

For example,

$ python cli.py config/chime-west.json --node chi01 --get power
$ python cli.py config/chime-west.json --node chi02 --get gpu_temperature
$ python cli.py config/chime-east.json --get air_temperature
$ python cli.py config/chime-east.json --get power
"""

import os,sys 

lib_path = os.path.dirname( os.path.realpath(__file__) ) + '/lib'
sys.path.append(lib_path)

from chime_monitor import *

# It's pretty important that you put a rack config file
# because otherwise we have no idea how the system is 
# configured.

if len(sys.argv) < 2:
	print "You need to specify a rack config file."
	print help
	sys.exit(0)

arguments = sys.argv[1:]

# Let's try to load the rack, based upon their config file.

rack_config = arguments[0]
try:
	rack = RackMachine(arguments[0])
except:
	print "There was a problem loading the rack configuration file '%s'. " % arguments[0]
	print help
	sys.exit(0)

# Now let's try to load up all of the configuration files.
node 		= None
data_name 	= None

for i in range(len(arguments)-1):
	if arguments[i] == "--node":
		node 		= arguments[i+1] 
	elif arguments[i] == "--get":
		data_name 	= arguments[i+1]

if node != None:
	try: 
		machine = rack.get_machine_by_hostname(node)
		if machine == None:
			raise Exception()
	except:
		print "There was a problem with your hostname '%s'. Refer to your configuration file." % node
		print help
		sys.exit(0)

if data_name == "power":
	rack.collect_power_statistics()
	print rack.statistics['power'][ int(machine.power_socket) ]
elif data_name == "gpu_temperature":
	for n,temp in enumerate(machine.gpu_temperature()):
		print "gpu_temperature%d:%d"%(n,temp),
else:
	print "Uknown command. Refer to help."
	print help
	sys.exit(0)
