import os,sys,time

lib_path = os.path.abspath('./lib')
sys.path.append(lib_path)

from chime_monitor import *

# Configuration: here, you can set up the way that 
# the CHIME monitor object will define itself. The
# key is to register RackMachine objects which
# point to configuration files.

chime_monitor = CHIMEMonitor()

# Create an instance of the chime_east rack based upon
# the JSON configuration file "config/chime-east.json".
chime_east = RackMachine('config/chime-east.json')

# Register the machine into the chime_monitor object, so that 
# it will be interrogated periodically and interfaced with.
chime_monitor.register_machine( chime_east )

# If you want to monitor more racks, just create another rack
# object based upon another JSON configuration file

console_title = """
   ____ _   _ ___ __  __ _____   __  __             _ _             
  / ___| | | |_ _|  \/  | ____| |  \/  | ___  _ __ (_) |_ ___  _ __ 
 | |   | |_| || || |\/| |  _|   | |\/| |/ _ \| '_ \| | __/ _ \| '__|
 | |___|  _  || || |  | | |___  | |  | | (_) | | | | | || (_) | |   
  \____|_| |_|___|_|  |_|_____| |_|  |_|\___/|_| |_|_|\__\___/|_|   
"""

# The program runs forever, once started.
while True:
	# Clear the terminal, and print out the title and render the 
	# status of the racks.
	print chr(27) + "[2J"
	print console_title
	print chime_monitor.render() + "\n"
	print chime_monitor.visualize() + "\n"
	print "System status: "
	print chime_east.status.__str__() + "\n"

	# In this step, we let the system react
	# to problems it has encountered in the previous step. The reactions
	# for different machine types are defined in `machine.py`
	chime_monitor.take_action()

	# Now we interrogate each computer in parallel, to determine all of
	# the statistics of each machine. After we're done that, the update() function
	# also applies all posssible failure modes to each machine.
	print "Collecting data from servers..."
	chime_monitor.update()

	# Wait for a while between interrogations, so we don't overload the computers
	# with requests for temperature, power, etc.
	time.sleep(10)
