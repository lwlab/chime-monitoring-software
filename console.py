import os,sys 

lib_path = os.path.abspath('./lib')
sys.path.append(lib_path)

from chime_monitor import *

chime_monitor = CHIMEMonitor()

chime_west = RackMachine('config/chime-west.json')
chime_east = RackMachine('config/chime-east.json')

chime_monitor.register_machine( chime_west )
chime_monitor.register_machine( chime_east )
chime_monitor.const['debug'] = True

print chime_west.show_power_layout()

print chime_west.visualize()
