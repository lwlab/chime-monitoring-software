import time

class FailureWarning(object):
	def __init__(self, descriptor=""):
		self.descriptor = descriptor
		self.logs 		= []
		self.logs.append("%s created at %s" % (type(self), time.asctime()))

	def __str__(self):
		return "%s : %s" % (type(self), self.descriptor)

	def severity(self):
		return type(self).SEVERITY

	def __repr__(self):
		return self.__str__()

	def name(self):
		return self.NAME

	# This function absorbs one failure warning into another one, so that
	# a bunch of errors can be spat out at once but retain the worst severity
	# warning on top.
	def absorb(self, failurewarning):
		if self.severity() < failurewarning.severity():
			raise Exception("It's illegal to merge a bad warning into a good warning.")

		if self.severity() == failurewarning.severity() and self.severity() != FailureWarning.SAFE.SEVERITY:
			self.descriptor = self.descriptor + "\n and " + failurewarning.descriptor
		else:
			self.logs.append( failurewarning.descriptor )
		for l in failurewarning.logs:
			self.logs.append(l)


class TransitionFailureWarning(FailureWarning):
	SEVERITY = 0
	NAME = "Transition"

	def __str__(self):
		return "%s : System is in transition (e.g. booting) " % type(self)

class SafeFailureWarning(FailureWarning):
	SEVERITY = 0
	NAME = "Safe"

	def __str__(self):
		return "%s : Everything looks normal." % type(self)

class SuspiciousFailureWarning(FailureWarning):
	SEVERITY = 1
	NAME = "Suspicious"

class MildFailureWarning(FailureWarning):
	SEVERITY = 2
	NAME = "Mild"

class MediumFailureWarning(FailureWarning):
	SEVERITY = 3
	NAME = "Medium"

class BadFailureWarning(FailureWarning):
	SEVERITY = 4
	NAME = "Bad"

class CrazyFailureWarning(FailureWarning):
	SEVERITY = 5
	NAME = "Crazy"

FailureWarning.SAFE 		= SafeFailureWarning
FailureWarning.TRANSITION	= TransitionFailureWarning
FailureWarning.SUSPICIOUS	= SuspiciousFailureWarning
FailureWarning.MILD 		= MildFailureWarning
FailureWarning.MEDIUM 		= MediumFailureWarning
FailureWarning.BAD 			= BadFailureWarning
FailureWarning.CRAZY 		= CrazyFailureWarning

# The failure mode object describes a generic failure mode. 
# A failure mode is applied to a machine. Based upon the measurements
# of the machine's health, the failure mode will report the machine's
# status. 
class FailureMode(object):
	def __init__(self, machine):
		self.machine = machine
	# Below are the different failure severities.
	def evaluate(self, machine):
		return FailureMode.SAFE

# This failure mode checks for overheating. It returns a failure warning,
# with some details about what went wrong. Based upon Keith's explanations.
class GPUOverheatingFailureMode(FailureMode):
	def evaluate(self, machine):
		try: 
			# We'll grab the highest possible temperature, and convert
			# it to float. If something goes wrong here, we'll except with
			# a "SUSPICIOUS" indication.
			temperature = float(max(machine.statistics['gpu_temperatures']))
		except:
			machine.log("Unable to read temperature!")
			return FailureWarning.SUSPICIOUS("Unable to read temperature of machine %s" % machine)
		if temperature < 20:
			return FailureWarning.SUSPICIOUS("GPU temperature (%s) strangely low." % temperature)
		if temperature > 100:
			return FailureWarning.BAD("GPU temperature (%s) too high!" % temperature)
		elif temperature > 90:
			return FailureWarning.MEDIUM("GPU temperature (%s) quite high!" % temperature)

		elif temperature > 80:
			return FailureWarning.MILD("GPU temperature (%s) uncomfortably high." % temperature)

		return FailureWarning.SAFE()

# This failure mode corresponds to the internal rack air being
# too hot. It applies specifically to RackMachines.
class PlenumOverheatingFailureMode(FailureMode):
	def evaluate(self, machine):
		try: 
			# We'll grab the highest possible temperature, and convert
			# it to float. If something goes wrong here, we'll except with
			# a "SUSPICIOUS" indication.
			temperature = float(machine.statistics['lower_temperature'])
		except:
			machine.log("Unable to read temperature!")
			return FailureWarning.SUSPICIOUS("Unable to read temperature of rack %s" % machine)
		
		if temperature < 20:
			return FailureWarning.SUSPICIOUS("%s : Rack air temperature (%s) is strangely low." % (machine,temperature))
		if temperature > 80:
			return FailureWarning.CRAZY("%s : Rack air temperature (%s) is extremely high!" % (machine,temperature))
		elif temperature > 70:
			return FailureWarning.BAD("%s : Rack air temperature (%s) is very high." % (machine,temperature))
		elif temperature > 65: 
			return FailureWarning.MEDIUM("%s : Rack air temperature (%s) is high." % (machine,temperature))
		elif temperature > 60:
			return FailureWarning.MILD("%s : Rack air temperature (%s) is slightly high." % (machine,temperature))

		else:
			return FailureWarning.SAFE()

# This failure mode was detected during startup, wherein
# computers would start, connect via SSH, but be unable
# to read graphics cards (and only one adapter shows up),
# shortly thereafter exhibiting kernel panic.
class GPUComputerNetherLandFailureMode(FailureMode):
	def evaluate(self, machine):
		try:
			valid = float(machine.statistics['validated'])
		except:
			return FailureWarning.SUSPICIOUS("Unable to read validation state of machine %s" % machine)

		# If the computer is not connected to the network,
		# it is not in nether-state.
		if valid == False:
			return FailureWarning.SAFE()

		try:
			gpus_count	= len(machine.statistics['gpus_connected'])
		except:
			return FailureWarning.MILD("Nether-land state detected on %s" % machine)

		# If fewer than three GPUs are connected, 
		if gpus_count < 3:
			return FailureWarning.MILD("Nether-land state detected on %s" % machine)

		# If the computer IS connected, but doesn't respond to GPU queries,
		# it is likely to be in nether-state.
		try: 
			temperature = float(max(machine.statistics['gpu_temperatures']))
		except:
			return FailureWarning.SUSPICIOUS("GPU temperature can't be read, but computer is connected: %s" % machine)


		return FailureWarning.SAFE()

class ComputerNotConnectedFailureMode(FailureMode):
	def evaluate(self, machine):
		try:
			valid = float(machine.statistics['validated'])
		except:
			return FailureWarning.MILD("Unable to read validation state of machine %s" % machine)

		if valid == False:
			return FailureWarning.MILD("%s may not be connected to the network. " % machine)

		return FailureWarning.SAFE()


class ComputerPowerDrawFailureMode(FailureMode):
	def evaluate(self, machine):
		try: 
			# We'll grab the power draw, and convert
			# it to float. If something goes wrong here, we'll except with
			# a "SUSPICIOUS" indication.
			power = float(machine.statistics['power'])
		except:
			machine.log("Unable to read machine power!")
			return FailureWarning.SUSPICIOUS("Unable to read power of machine %s" % machine)
			
		if power < 50:
			return FailureWarning.SUSPICIOUS("%s : Power draw (%s) is strangely low" % (machine,power))	

		if power > 900:
			return FailureWarning.BAD("%s : Power draw (%s) is far too high" % (machine,power))
		elif power > 850:
			return FailureWarning.MEDIUM("%s : Power draw (%s) is very high" % (machine,power))
		elif power > 800:
			return FailureWarning.MILD("%s : Power draw (%s) is strangely high" % (machine,power))

		return FailureWarning.SAFE()

# This is defined specifically for RackMachines, and measures
# the airflow in the rack.
class AirflowFailureMode(FailureMode):
	def evaluate(self, machine):
		try: 
			# We'll grab the power draw, and convert
			# it to float. If something goes wrong here, we'll except with
			# a "SUSPICIOUS" indication.
			flow_rate = float(machine.statistics['litres_per_second'])
		except:
			machine.log("Unable to read machine power!")
			return FailureWarning.SUSPICIOUS("Unable to read power of rack %s" % machine)
		
		if flow_rate < 10:
			return FailureWarning.SUSPICIOUS("%s : Fans are operating too slowly: %s L/s" % (machine,flow_rate))

		return FailureWarning.SAFE()