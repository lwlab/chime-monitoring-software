# machine.py
# Author: Colin Merkel

import subprocess, threading, re, json, shell_command
import sys, pycurl, time
import urllib, urllib2, base64
from StringIO import StringIO
from failure_modes import *

# A Machine corresponds to a computer.
# This class abstracts away the interface
# to that computer, whether it be a local
# computer, CHI GPU machine, or otherwise.
class Machine(object):
	FAILUREMODES = [ ComputerPowerDrawFailureMode ]

	def __init__(self, hostname, parent=None):
		self.hostname 				= hostname
		#self.validated_at_startup 	= self.validate()
		self.parent 				= parent
		self.power_socket			= None
		self.rack 					= None
		self.statistics 			= {}
		self.status 				= FailureWarning.SUSPICIOUS("Status not yet checked.")
		# By default, start in a transition state.
		self.start_transition(1)

	# Return a string representing the machine.
	def render(self):
		return "%s@%s [%s]" % ( type(self), self.hostname, self.short_status() )

	def update(self):
		self.collect_statistics()
		self.evaluate()

	# This function is a way to access the failure modes defined
	# for a particular machine type.
	def failuremodes(self):
		if hasattr(self, 'FAILUREMODES'): 
			return type(self).FAILUREMODES
		else:
			return []

	# This function puts the machine into "transition" state, 
	# which blocks it from reporting erroneous transition states
	def start_transition(self, timeout=150): 
		self.transition_expiry = int(time.time() + timeout)

	def in_transition(self):
		return int(time.time()) < self.transition_expiry
	
	def visualize(self):
		return self.render()

	def action_log(self, message):
		print "[ACTION %s] %s" % (time.asctime(), message)

	def action(self):
		if self.status.SEVERITY == FailureWarning.MILD.SEVERITY:

			# Double-check the problem, to make sure it isn't
			# just a temporary fluke.
			self.collect_statistics()
			self.evaluate()
			# If our new state is less bad, let's not worry about
			# taking any actions.
			if self.status.SEVERITY < FailureWarning.MILD.SEVERITY:
				return

			self.action_log("Restarting %s, because severity is MILD." % self)
			self.pull_plug()
			self.plug_in()
			# Now set the state to "transitionary".
			self.start_transition()

	# This function evaluates itself against the possible failure
	# modes that are defined for it.
	def evaluate(self):
		# First of all, if the machine is in transition state, 
		# we want to avoid reporting failure warnings.
		if self.in_transition() == True:
			self.status = FailureWarning.TRANSITION()
			return self.status

		status = FailureWarning.SAFE()

		for f in self.failuremodes():
			mode = f(self)
			result = mode.evaluate(self)

			# If the observed result is worse than the 
			# current result, we'll only report the worse of the
			# two results.
			if result.SEVERITY > status.SEVERITY:
				result.absorb(status)
				status = result
			else:
				status.absorb(result)
		self.status = status
		return status

	# Returns the status in short form.
	def short_status(self):
		return self.status.name()

	# Determines how logs are handled. If there is a parent (e.g. a rack)
	# then we send the logging to the parent to decide what to do with it.
	def log(self, message):
		if hasattr(self, 'parent') == False or self.parent is None:
			pass # print message
		else:
			self.parent.log(message)

	# This function collects all relevant statistics for the machine.
	# The generic "Machine" object doesn't have any statistics, except
	# whether it is connected to the network.
	def collect_statistics(self):
		self.statistics = {}
		self.statistics['validated'] = self.validate()
		return True

	# This function kills all of the energy-consuming
	# processes on the machine (and possibly restarts them).
	def killall(self):
		return True

	# Unplug the computer.
	def pull_plug(self):
		if self.rack == None:
			return False
		else:
			return self.rack.pull_plug(self.power_socket)

	# Restore power to the computer.
	def plug_in(self):
		if self.rack == None:
			return False
		else:
			return self.rack.plug_in(self.power_socket)

	# This function "somehow" validates the connection
	# to the device, and returns true or false based
	# upon the validation result.
	def validate(self):
		# We'll use an SSH echo test.
		result = self.cli_interface("echo test123", timeout=2)
		if result == "test123\n":
			self.log("Validation for %s passed." % self.render())
			return True
		else:
			self.log("Validation for %s failed. Received '%s', want 'test123'." % (self.render(), result))
			return False

	# The SSH-interface command allows the owner
	# of a machine to speak to the machine. The argument
	# should be a command to run on the machine (e.g. via SSH)
	# and the response is the result of the command.
	def cli_interface(self, command, timeout=3):
		try: 
			return shell_command.shell_command("ssh root@%s '%s'" % (self.hostname, command), timeout)
		except Exception:
			self.log("Process terminated due to timeout.")

		return ""

	# Tell the computer to shut down. NOTE: this function may not work because
	# the machines seem to crash at the last stages of shutdown.
	def shutdown(self):
		self.cli_interface('shutdown -h now')

	# Tell the computer to restart. NOTE: this function may not work because
	# the machines seem to crash at the last stages of shutdown.
	def restart(self):
		self.cli_interface('shutdown -r now')

	def __str__(self):
		return self.render()

	def __repr__(self):
		return self.__str__()

class GPUMachine(Machine):
	FAILUREMODES = [ GPUOverheatingFailureMode, ComputerPowerDrawFailureMode, ComputerNotConnectedFailureMode, GPUComputerNetherLandFailureMode ]

	# This function talks to the machine and requests a temperature for
	# all of its adapters. It returns an array of temperatures. If something
	# goes wrong with one or more adapters, or it is unable to connect to
	# the machine, it doesn't return that adapter's temperature. If everything
	# is disconnected, the result is an empty array.
	def gpu_temperature(self):
		result = self.cli_interface('DISPLAY=:0 aticonfig --odgt --adapter=all')
		lines = result.split("\n")
		temperature_regex = re.compile('Sensor 0: Temperature - ([\d.]+) C')
		temperatures = []
		for i in range(len(lines)):
			if re.match('Adapter', lines[i]):
				# Need to double-check that there actually is a next line
				if i+1 < len(lines):
					# Detect the sensor temperature.
					temp = temperature_regex.search(lines[i+1])
					if temp: 
						temperature = temp.group(1)
						try:
							temperatures.append( float(temperature) )
						except:
							self.log("Malformed temperature '%s' received, want float" % temperature)
							continue
					else:
						self.log("Unable to match temperature after adapter...")
				else:
					self.log('Malformed GPU temperature response')

		return temperatures

	# The only internal statistics for a machine is its GPU temperatures.
	def collect_statistics(self):
		# Don't bother collecting statistics when in transition.
		if self.in_transition():
			return True

		self.statistics['validated'] = self.validate()
		if self.statistics['validated']:
			self.statistics['gpus_connected'] 	= self.gpus_connected()
			self.statistics['gpu_temperatures'] = self.gpu_temperature()

	# This function returns the highest-temperature GPU.
	def max_gpu_temperature(self):
		return max(self.gpu_temperature())

	def gpus_connected(self):
		data = self.cli_interface('ls /dev/ati')
		return data.split()

	# This function kills the correlator program.
	def killall(self):
		self.cli_interface('killall correlator')
		# Eventually, we might want to re-start the correlator here, but
		# we'll need the details of where the software is located first.

	# This function returns the difference in temperatures
	# of the GPUS. If it is very high, maybe something is wrong.
	def gpu_temperature_range(self):
		temperatures = self.gpu_temperature()
		return max(temperatures) - min(temperatures)

class LocalMachine(Machine):

	FAILUREMODES = []

	def __init__(self, parent=None):
		self.hostname = "localhost"
		self.parent	= parent
		super(LocalMachine, self).__init__(self.hostname, parent)

	def cli_interface(self, command, timeout=3):
		try: 
			return shell_command.shell_command(command, timeout)
		except Exception:
			self.log("Process terminated due to timeout.")

		return ""

	# Local machines shouldn't be shut down - they are 
	# running this application!
	def shutdown(self):
		return True

# A RackMachine is an entire rack of machines. 
class RackMachine(Machine):
	FAILUREMODES = [ PlenumOverheatingFailureMode, AirflowFailureMode ]

	# Racks have names, not hostnames.
	def __init__(self, filename):
		self.machines = []
		self.status = FailureWarning.SAFE()
		self.load_rack_from_file(filename)
		super(RackMachine, self).__init__(self.hostname, None)

	def update(self):
		for m in self.machines:
			m.collect_statistics()
			m.evaluate

	# Return the machine object for a machine with a hostname
	# matching the argument. If none can be found, it returns None.
	def get_machine_by_hostname(self, hostname):
		for m in self.machines:
			if hostname == m.hostname:
				return m
		else:
			return None

	# This function pulls the plug of a socket.
	def set_power_for_socket(self, power_socket, state=True):
		params = {}
		params['actLoadId'] = power_socket
		if state == True:
			params['loadon1'] 	= "On"
		else:
			params['loadoff1']	= "Off"

		params = urllib.urlencode(params)
		#params += "&load1=&load2=&load3=&load4=&load5=&load6=&load7=&load8=&load9=&load10=&load11=&load12=&load13=&load14=&load15=&load16=&load17=&load18=&load19=&load20=&load21=&load22=&load23=&load24=&load25=&load26=&load27=&load28=&load29=&load30=&load31=&load32=&load33=&load34=&load35=&load36=&load37=&load38=&load39=&load40=&load41=&load42=&load43=&load44=&load45=&load46=&load47=&load48=&load49=&load50=&load51=&load52=&load53=&load54=&load55=&load56=&load57=&load58=&load59=&load60=&load61=&load62=&load63=&load64="

		password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
		top_level_url = "http://%s" % self.pdu_hostname
		password_mgr.add_password(None, top_level_url, 'admin', 'admin')
		handler = urllib2.HTTPBasicAuthHandler(password_mgr)
		opener = urllib2.build_opener(urllib2.HTTPHandler, handler)
		urllib2.install_opener(opener)
		request = urllib2.Request("http://%s:80/Forms/action_load_list_1" % self.pdu_hostname, params)
		try:
			response = urllib2.urlopen(request)
		except:
			self.log("Unable to connect to PDU to execute power switch command.")
		return True

	# This function looks at the state of the machine, and takes 
	# whatever actions are necessary to correct the state. For example,
	# if the machine is in MILD state, it might restart itself. In the rack,
	# we delegate the action step down to each individual node.
	def action(self):
		for m in self.machines:
			m.action()

	# Unplug an individual machine in the rack,
	# identified by power socket. Be careful with this
	# one: it is possible to unplug the root node.
	def pull_plug(self, power_socket):
		self.set_power_for_socket(power_socket, False)
		time.sleep(2)
		self.set_power_for_socket(8, False)
		time.sleep(2)

	# Plug in an individual machine in the rack,
	# identified by power socket.
	def plug_in(self, power_socket):
		self.set_power_for_socket(power_socket, True)
		time.sleep(2)
		self.set_power_for_socket(8, True)
		time.sleep(2)
			
	# Unplug all machines in the rack.
	def unplug_all(self):
		for m in self.machines:
			m.pull_plug()

	# Plug in all machines in the rack.
	def plug_in_all(self):
		for m in self.machines:
			m.plug_in()

	# A rack is validated if all of its machines are validated.
	def validate(self):
		for m in self.machines:
			if m.validate() == False:
				return False
		return True

	# Returns a string visualizing the rack and its current state.
	def visualize(self):
		out = []
		out.append("%s : %s [%s]" % (type(self), self.hostname, self.short_status()))
		for m in self.machines:
			out.append(" - %s" % m)
		return "\n".join(out)

	# To register a machine with the rack, pass it in
	# to this function.
	def register_machine(self, machine):
		machine.rack = self
		self.machines.append(machine)

	# This function overrides the "evaluate" behavior by 
	# first running the 'super' evaluate, and then
	# doing so for each of its constituent machines.
	def evaluate(self):
		status = super(RackMachine, self).evaluate()
		for m in self.machines:
			result = m.evaluate()

			if result.SEVERITY > status.SEVERITY:
				result.absorb(status)
				status = result
			else:
				status.absorb(result)

		self.status = status
		return self.status

	# This function loads the a rack specification from 
	# a JSON file.
	def load_rack_from_file(self, filename):
		json_data = open(filename)
		data = json.load(json_data)
		json_data.close()

		if 'name' in data: 			self.hostname 	= str(data['name'])
		if 'description' in data: 	self.description= str(data['description'])

		if 'fans_hostname' in data == False:
			raise Exception("The specified JSON file '%s' lacks a definition of the fans hostname, which is required. %s" % (filename, data))

		self.fans_hostname = str(data['fans_hostname'])

		if 'pdu_hostname' in data == False:
			raise Exception("The specified JSON file '%s' lacks a definition of the PDU hostname, which is required. %s" % (filename, data))

		self.pdu_hostname = str(data['pdu_hostname'])

		if 'gpu_machines' in data == False:
			raise Exception("The specified JSON file '%s' has no 'gpu_machines' array, which is required. %s" % (filename, data))

		for g in data['gpu_machines']:
			if 'hostname' in g == False or 'power_socket' in g == False:
				raise Exception("The specified JSON file '%s' has a malformed gpu_machine description" % filename)
			gpumachine = GPUMachine(str(g['hostname']))
			gpumachine.power_socket = str(g['power_socket'])
			self.register_machine( gpumachine )

	# A shutdown command shuts down all machines within
	# a rack.
	def shutdown(self):
		for m in self.machines:
			m.shutdown()

	def restart(self):
		for m in self.machines:
			m.restart()

	# Using the CLI interface on a rack is illegal.
	def cli_interface(self, command, timeout=3):
		return False

	# Returns a string demonstrating the system power layout
	def show_power_layout(self):
		out = []
		out.append( "Power layout for %s@%s" % ( type(self), self.hostname ) )
		for m in self.machines:
			if m.power_socket == None:
				m.power_socket = "?"
			out.append(" - %s  ->  power socket #%s" % (m, m.power_socket))

		return "\n".join(out)

	# This function collects all relevant statistics for the 
	# machine. For the rack, it collects fan and power data,
	# and sends the power data down to its GPUMachines so they
	# can think about their individual power consumptions.
	def collect_statistics(self):
		self.statistics = {}		
		self.collect_fan_statistics()
		self.collect_power_statistics()

		class DataCollectionThread(threading.Thread):
			def __init__(self, machine):
				threading.Thread.__init__(self)
				self.machine = machine

			def run(self):
				self.machine.collect_statistics()
				#print "Thread for %s is complete." % self.machine

		threads = []

		for m in self.machines:
			threads.append( DataCollectionThread(m) )

		for t in threads:
			t.start()

		# Wait for all threads to finish.
		for t in threads:
			t.join()

		self.log("All threads finished.")

		for m in self.machines:
			# Update the power statistics for the machine, based upon
			# the machine's power socket, etc.
			try:
				m.statistics['power'] = self.statistics['power'][int(m.power_socket)]
			except:
				# If we can't access a machine's power, that's not an
				# exception on its own, because the machine could be 
				# running without a rack.
				m.statistics['power'] = None

	def collect_power_statistics(self):
		# PyCURL uselessly writes the output of the 
		# CURL out to standard output, so we need to 
		# capture that using a wrapper of some kind.

		# The server sometimes (randomly?) responds with an "access denied" message. It seems like this
		# is always the first time we try to access the file, after which it behaves nicely. So we will
		# attempt to get the data from the server twice. If it works the first time, great. If not, we'll
		# try again. If it fails twice, we need to make sure we return nothing in order to not give invalid
		# data.

		# Start with the assumption of bad data.
		data_is_valid = False

		for i in range(0,3):
			storage = StringIO()
			c = pycurl.Curl()
			c.setopt(pycurl.URL, "http://admin:admin@%s/logs/datalog.csv" % self.pdu_hostname)
			# I believe that the target server ignores the pycurl.RANGE argument and just
			# returns the whole damn thing. Therefore I've commented out the following line,
			# which would limit the amount of data we would pull from the server.
			#	c.setopt(pycurl.RANGE, "0-500")
			try:
				c.setopt(pycurl.WRITEFUNCTION, storage.write)
				c.perform()
				c.close()
			except:
				return
			
			result = storage.getvalue()
			# After some testing, I've noticed that the "real" file has no instances of the greater-than
			# or less-than characters. So by counting those characters, we can determine that the file is
			# either valid or invalid. The invalid file is HTML, and is full of those characters (14 in total). 
			# Therefore we can set a threshold of 5 invalid characters before we re-request the data.
			#print "I found %d instances of '<' in this file." % result.count('<')
			if result.count('<') < 5:
				# The data is valid. We don't have to try to download it again.
				data_is_valid = True
				break

		# If we don't have valid data now, we must quit silently.
		if data_is_valid == False:
			self.statistics['power'] = []
			return

		# the [8] index contains the first line of data, which is
		# the most recent.

		lines = result.split("\n")

		headers = lines.pop(0).split(",")
		headers.append("N/A")

		rows = []

		for line in lines:
			data = {}
			line = line.split(",")
			# For each element in the row, add to the current element.
			for i in range(0,len(line)):
				if i < len(headers):
					data[ headers[i].strip() ] = line[i].strip()
			# Add to the rows
			rows.append( data )

		# We need to use the second-last row, for some reason. The last row is junk.
		most_recent_data = rows[-2]

		# There actually is no "zeroth" plug. Therefore the zeroth plug
		# has zero power draw.
		output_power = [0]
 		for i in range(1,25):
			output_power.append( int(most_recent_data['Load Power %d'%i].strip(' W')) )

		self.statistics['power'] = output_power

	def collect_fan_statistics(self):
		# PyCURL uselessly writes the output of the 
		# CURL out to standard output, so we need to 
		# capture that using a wrapper of some kind.
		storage = StringIO()

		c = pycurl.Curl()
		c.setopt(pycurl.URL, "ftp://apc:apc@%s/data.txt" % self.fans_hostname)
		c.setopt(pycurl.RANGE, "0-500")
		c.setopt(pycurl.WRITEFUNCTION, storage.write)
		try:
			c.perform()
			c.close()
		except:
			return
		result = storage.getvalue()

		# the [8] index contains the first line of data, which is
		# the most recent.
		data = result.split("\n")[8].split("\t")

		# The Columns are, in order:
		# date, time, percentage of full speed, cubic feet per minute, upper temperature, lower temperature
		response = {}
		self.statistics['percentage_full_speed']  = data[2]
		self.statistics['cubic_feet_per_minute']  = data[3]
		self.statistics['litres_per_second'] 	= int(float(data[3]) * 0.471947443)
		self.statistics['upper_temperature'] 	= data[4]
		self.statistics['lower_temperature']	= data[5]

		return True

	# Determines how to visualize the rack in a string.
	def render(self):
		return "%s@%s { %s }" % ( type(self), self.hostname, ', '.join( [ m.render() for m in self.machines ] ))

